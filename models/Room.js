const mongoose = require("mongoose")
const Schema = mongoose.Schema



const RoomSchema = new Schema({
    roomname: {
        type: String,
        required: [true, "Room name is required!"]
    },
    categoryId: {
        type: String,
        required: [true, "Category ID is required!"]
    },
    image: {
        type: String,
        required: [true, "Room image is required"]
    },
    price: {
        type: String,
        required: [true, "Price is required!"]
    },
    description: {
        type: String,
        required: [true, "Product description is required"]
    }
})


module.exports = mongoose.model("Room", RoomSchema)