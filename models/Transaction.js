const mongoose = require("mongoose")
const Schema = mongoose.Schema

const TransactionSchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    statusId: {
        type: String,
        default: "5e9eb2e60cf7a34fb863b113"
    },
    dateCreated: {
        type: Date,
        default: Date.now
    },
    paymentMode: {
        type: String,
        required: true
    },
    total: {
        type: Number,
        required: true
    },
    rooms: [
        {
            roomId: String,
            roomname: String,
            price: Number,
            checkIn: Date,
            checkOut: Date,
            bookingId: String,
            noOfGuest: Number,
            noOfDays: Number,
            subTotal: Number
        }
    ]
})

module.exports = mongoose.model("Transaction", TransactionSchema)