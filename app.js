const express = require("express")
const app = express()
const PORT = process.env.PORT ||  4000;
const cors = require("cors")
const mongoose = require("mongoose")

mongoose.connect("mongodb+srv://b49cp3:112233445566778899@b49-cp3-t9anz.mongodb.net/test?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})

const db = mongoose.connection
db.once('open', () => console.log("You are now connected on MongoDB"))

app.use(cors())
app.use(express.json())
app.use(express.static('public'))

//will define the routing
app.use("/users", require("./routes/users"))
app.use("/rooms", require("./routes/rooms"))
app.use("/categories", require("./routes/categories"))
app.use("/statuses", require("./routes/statuses"))
app.use("/transactions", require("./routes/transactions"))
app.use("/contacts", require("./routes/contacts"))

app.listen(PORT, () => console.log(`You are now connected to PORT : ${PORT}`))
