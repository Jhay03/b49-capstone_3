const Status = require("../models/Status")
const express = require("express")
const router = express.Router()
const isAdmin = require("../auth")

//add a room
router.post("/", isAdmin, (req, res) => {
    let status = new Status()
    status.name = req.body.name
    status.save()
    return res.status(200).json({message: "Status Successfully Added!", status:200})
})


//view all status
router.get('/', (req, res) => {
    Status.find({}, (err, statuses) => {
        return res.json(statuses)
    })
})
module.exports = router;