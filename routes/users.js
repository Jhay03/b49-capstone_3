const User = require("../models/User")
const express = require("express")
const router = express.Router()
const bcrypt = require("bcryptjs")
const validator = require("email-validator");
const jwt = require("jsonwebtoken")

//register localhost:4000/user
router.post("/", (req, res) => {
    //username must be greater than 8 characters
    if(req.body.username.length < 8) return res.status(400).json({status: 400, message: "Username must be greater than 8 characters"})
    
    //password must be greater than 8 characters
    if(req.body.password.length < 8) return res.status(400).json({status: 400, message: "Password must be greater than 8 characters"})

    //password must be equal to password2
    if(req.body.password != req.body.password2) return res.status(400).json({status: 400,   message: "Password doesn't match!"})

    //if(req.body.email != validator.validate("test@mail.com")) return res.status(400).json({status: 400, message: "Please input a valid email."})
    //check if number is greater than or equal to 11
    if (req.body.number.length < 10) return res.status(400).json({ status: 400, message: "Phone number must be greater than 11 digits" })
    
    //check if username is already exist.
    User.findOne({ username: req.body.username }, (err, user) => {
        if (user) return res.status(400).json({
            status: 400,
            message: "Username already exist!!"
        })
        bcrypt.hash(req.body.password, 10, (err, hashedPassword) => {
            const user = new User()
            user.fullname = req.body.fullname
            user.username = req.body.username
            user.password = hashedPassword
            user.email = req.body.email
            user.number = req.body.number
            user.save()
            return res.status(200).json({
                status: 200,
                message: "Registered Successfully"
            })

        })
    })
})

//login localhost:4000/login
router.post("/login", (req, res) => {
    User.findOne({ username: req.body.username }, (err, user) => {
        if(err || !user) return res.status(400).json({mesasge: "No user found!"})
        bcrypt.compare(req.body.password, user.password, (err, result) => {
            if (!result) {
                return res.status(401).json({
                    auth: false,
                    message: "Invalid credentials",
                    token: null
                })
            } else {
                let token = jwt.sign(user.toJSON(), 'b49-cp3', { expiresIn: '1h' })
                return res.status(200).json({
                    auth: true,
                    message: "Login Successfully!",
                    user,
                    token
                })
            }

        })
    })
})


module.exports = router