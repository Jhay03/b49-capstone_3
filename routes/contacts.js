const express = require("express")
const router = express.Router()
const Contact = require("../models/Contact")



//add a message
router.post("/", (req, res) => {
    const contact = new Contact()
    contact.fullname = req.body.fullname
    contact.email = req.body.email
    contact.message = req.body.message
    contact.save()
    return res.status(200).json({message: "Your message has been sent to the admin!", status:200})
})


//view all message
router.get("/", (req, res) => {
    Contact.find({}, (err, messages) => {
        return res.json(messages)
    })
})

//view message by id
router.post("/:id", (req, res) => {
    Contact.findOne({ _id: req.params.id }, (err, message) => {
        return res.json(message)
    })
})

//delete a message
router.delete("/:id", (req, res) => {
    Contact.findOneAndDelete({ _id: req.params.id }, (err, contact) => {
        return res.status(200).json({contact, message:"Message Successfully Deleted!"})
    })
})


module.exports = router;