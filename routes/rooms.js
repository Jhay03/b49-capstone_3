const Room = require("../models/Room")
const express = require("express")
const router = express.Router()
const multer = require("multer")
const isAdmin = require("../auth")

let storage = multer.diskStorage({
    destination: function (req, file, cb){
       cb(null, 'public/rooms')
    },
    filename: function (req, file, cb) {
        cb(null,new Date().getTime() + "-" + file.originalname)
    }
})

let upload = multer({ storage: storage })

//add a room
router.post("/", upload.single('image'), isAdmin, (req, res) => {
    let room = new Room()
    room.roomname = req.body.roomname
    room.categoryId = req.body.categoryId
    room.image = "/rooms/"+req.file.filename
    room.price = req.body.price
    room.description = req.body.description
    room.save()
    return res.status(200).json({message: "Room Successfully Added!", status:200})
})
//view all rooms
router.get('/', (req, res) => {
    Room.find({}, (err, rooms) => {
        return res.json(rooms)
    })
})
//view single room per ID
router.get("/:id", (req, res) => {
    Room.findOne({ _id: req.params.id }, (err, room) => {
        return res.json(room)
    })
})

//edit a room by id
router.put("/:id", upload.single('image'), isAdmin, (req, res) => {
    let updatedRoom = { ...req.body}
    if (req.file) {
        updatedRoom = {...req.body, image: "/rooms/" + req.file.filename}
    }
    Room.findOneAndUpdate(
        { _id: req.params.id },
        updatedRoom,
        { new: true },
        (err, updated) => {
            return res.status(200).json({updated, message: "Room updated Successfully", status: 200})
        }
    )

})

//delete a room
router.delete("/:id", isAdmin, (req, res) => {
    Room.findOneAndDelete({ _id: req.params.id }, (err, room) => {
        return res.status(200).json({room, message:"Room Successfully Deleted!"})
    })
})

module.exports = router;