const express = require("express")
const router = express.Router()
const Category = require("../models/Category")
const isAdmin = require("../auth")
const multer = require("multer")


let storage = multer.diskStorage({
    destination: function (req, file, cb){
       cb(null, 'public/categories')
    },
    filename: function (req, file, cb) {
        cb(null,new Date().getTime() + "-" + file.originalname)
    }
})

let upload = multer({ storage: storage })


//add a categories
router.post("/", upload.single('image'), isAdmin, (req, res) => {
    let category = new Category()
    category.name = req.body.name
    category.image = "/categories/"+req.file.filename
    category.save()
    return res.status(200).json({message: "Category Successfully Added!", status:200})
})

//localhost:4000/caegories
router.get("/", (req, res) => {
    Category.find({}, (err, categories) => {
        return res.json(categories)
    })
})

//localhost:4000/categories/${id}
//view single room per ID
router.get("/:id", (req, res) => {
    Category.findOne({ _id: req.params.id }, (err, category) => {
        return res.json(category)
    })
})

module.exports = router