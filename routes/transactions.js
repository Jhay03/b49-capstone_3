const express = require("express")
const router = express.Router()
const Room = require("../models/Room")
const Transaction = require("../models/Transaction")
const User = require("../models/User")
const isAdmin = require("../auth")
const jwt = require("jsonwebtoken")
const stripe = require('stripe')('sk_test_1e06Kd7efHuWpXL2uZrlImbY00lIjcpnra')

//add a transaction
router.post("/", (req, res) => {
    //console.log(req.body)
    //if(req.body.noOfGuest.length < 0) return res.status(400).json({status: 400, message: "No of guest minimum value of 1"})
    //if(req.body.noOfGuest.value < 6) return res.status(400).json({status: 400, message: "4-6 per person rooms are only allowed"})
    let token = req.headers['x-auth-token']
    if (!token) return res.status(401).json({ message: "You are not logged in!", status: 401 })
    
    let decoded = jwt.verify(token, 'b49-cp3')
    if (decoded) {
        let transaction = new Transaction()
        transaction.userId = decoded._id
        transaction.paymentMode = "Pay before check-in"
        transaction.total = req.body.total
        transaction.rooms = req.body.orders
        //console.log(transaction)
        transaction.save()
        return res.status(200).json({ mesasge: "Transaction successful" });
    }
})

//localhost:4000/stripes
router.post("/stripe", (req, res) => {
    //console.log(req.body)
    let token = req.headers['x-auth-token'];
    if (!token) return res.status(401).json({ message: "You are not logged in.", status: 401 })
    
    let decoded = jwt.verify(token, 'b49-cp3')
    if (decoded) {
        let transaction = new Transaction()
        transaction.userId = decoded._id
        transaction.paymentMode = "Stripe"
        transaction.total = req.body.amount
        transaction.rooms = req.body.bookItems
        //console.log(transaction)
       transaction.save()

     
        let body = {
            source: req.body.token.id,
            amount: req.body.amount,
            email: req.body.email,
            currency: "PHP"
        }

        stripe.charges.create(body, (err, result) => {
            if (result) {
                transaction.save()
                return res.json({status:200, message:"Transaction Successful"})
            } else {
                return res.send(err)
            }
        })

    }
})

//get by ID


//get all transactions
router.get("/", (req, res) => {
    Transaction.find({}, (err, transactions) => {
        return res.json(transactions)
    })
})


//edit all transactions by the admin
router.put("/:id",  (req, res) => {
    Transaction.findOne({_id: req.params.id }, (err, transaction) => {
        transaction.statusId = req.body.statusId
        transaction.save()
        return res.json({status: 200, message: "Status Changed!"})
    })
})

router.get("/:userId", (req,res) => {
    let token = req.headers['x-auth-token']
    if (!token) return res.json({ status: 401, message: "You are not logged in!" })
    {/*, (err, transactions) =>*/}
    Transaction.find({ userId: req.params.userId })
    .then(transaction => res.json(transaction))
})

module.exports = router;